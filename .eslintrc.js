module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production1' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production1' ? 'error' : 'off',
    'comma-dangle': 'off',
    semi: 'off',
    'import/extensions': 'off',
    'no-return-assign': 'off',
    'no-unused-vars': 'off',
    'class-methods-use-this': 'off',
    'prefer-destructuring': 'off',
    'prefer-promise-reject-errors': 'off',
    'no-param-reassign': 'off',
    'arrow-body-style': 'off',
    'no-plusplus': 'off',
    'max-len': ['error', { code: 150 }],
    'vue/script-indent': ['error', 2, { baseIndent: 1 }],
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  overrides: [
    {
      files: ['*.vue'],
      rules: {
        indent: 'off'
      }
    },
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
      ],
      env: {
        mocha: true,
      },
    },
  ],
};
