const menu = [
  {
    path: 'me',
    component: () => import('@/views/UserProfile'),
    name: 'UserProfile',
    meta: {
      title: 'User Profile',
      icon: 'mdi-account',
    }
  },
  {
    path: 'create/community',
    component: () => import('@/views/NewCommunity'),
    name: 'NewCommunity',
    meta: {
      title: 'New Community',
    }
  },
  {
    path: '/app/create/post',
    name: 'NewPost',
    component: () => import('@/views/CreatePost'),
    meta: {
      title: 'Create post'
    }
  },
  {
    path: '/app/:communitySlug/:postSlug',
    name: 'Post',
    component: () => import('@/views/Post'),
    meta: {
      title: 'Post detail'
    }
  },
  {
    path: 'community/:communitySlug',
    component: () => import('@/views/Community'),
    name: 'Community',
    meta: {
      title: 'Community',
    },
    children: [
      {
        path: 'manage',
        name: 'Community_Manage',
        component: () => import('@/components/do/CommunityManage'),
        children: [
          {
            path: 'tag/new',
            name: 'Community_New_Tag',
            component: () => import('@/components/do/CommunityTag')
          },
          {
            path: 'tag/:tagId',
            name: 'Community_Tag',
            component: () => import('@/components/do/CommunityTag')
          },
        ]
      },
    ]
  },
]

const routes = [
  {
    path: '/',
    name: 'Root',
    redirect: { name: 'Home' }
  },
  {
    path: '/app',
    name: 'Home',
    meta: {
      title: 'Home',
    },
    component: () => import('@/views/Home'),
    children: [...menu],
  },
  {
    path: '/app/login',
    name: 'Login',
    component: () => import('@/views/Login'),
    meta: {
      title: 'Login'
    }
  },
  {
    path: '/app/register',
    name: 'Register',
    component: () => import('@/views/Register'),
    meta: {
      title: 'Register'
    }
  },
  {
    path: '/auth/oauth2/:provider/callback',
    component: {
      template: '<div class="auth-component"></div>'
    }
  },
]

export default routes
export { menu }
