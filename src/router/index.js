import Vue from 'vue'
import Router from 'vue-router'

// Routes
import routes from './paths'

Vue.use(Router)

// Create a new router
const router = new Router({
  mode: 'history',
  routes: [
    ...routes,
    { path: '*', redirect: '/' }
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    }
    if (to.hash) {
      return { selector: to.hash }
    }
    return { x: 0, y: 0 }
  }
})

router.beforeEach((to, from, next) => {
  document.title = `DO | ${to.meta.title}`
  next()
})

export default router
