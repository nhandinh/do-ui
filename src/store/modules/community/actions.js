import communityApi from '@/api/community'

export default {
  loadCurrentCommunity({ commit }, slug) {
    return communityApi.getBySlug(slug)
      .then((response) => {
        commit('setCurrentCommunity', response.data.data)
        return Promise.resolve({})
      })
  },
  clearCurrentCommunity({ commit }) {
    commit('setCurrentCommunity', null)
  },
}
