import { set } from '@/utils/vuex'

export default {
  setCurrentCommunity: set('currentCommunity'),
}
