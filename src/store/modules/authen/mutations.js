import { set } from '@/utils/vuex'

export default {
  setAuthenticated: set('authenticated'),
  setUserInfo: set('userInfo'),
  setUserRole: set('userRole'),
}
