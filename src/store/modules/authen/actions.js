import authenApi from '@/api/authen'

export default {
  localLogin({ commit }, credentials) {
    return authenApi.localLogin(credentials)
      .then((response) => {
        const token = response.data.data.token
        localStorage.setItem('token', token)
        commit('setAuthenticated', token)
        return authenApi.whoAmI()
      })
      .then((response) => {
        commit('setUserInfo', response.data.data)
        return Promise.resolve({})
      })
  },
  socialLogin({ commit }, { provider, code }) {
    return authenApi.socialLogin(provider, code)
      .then((response) => {
        const token = response.data.data.token
        localStorage.setItem('token', token)
        commit('setAuthenticated', token)
        commit('setUserInfo', response.data.data)
      })
  },
  logout({ commit }) {
    localStorage.removeItem('token')
    commit('setAuthenticated', false)
    commit('setUserInfo', null)
    return Promise.resolve({})
  },
  getAccountPermission() {
  }
}
