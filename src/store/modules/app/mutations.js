import { set, toggle } from '@/utils/vuex'

export default {
  setDrawer: set('drawer'),
  setReady: set('ready'),
  setImage: set('image'),
  setColor: set('color'),
  toggleDrawer: toggle('drawer')
}
