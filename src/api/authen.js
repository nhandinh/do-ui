import BaseApi from '@/api/base-api'

class AuthenApi extends BaseApi {
  constructor() {
    super('/auth')
  }

  localLogin(credentials) {
    // return Promise.resolve({ data: { data: { token: 'abcd' } } })
    // return Promise.reject(new Error('err'))
    return this.sendPost({ subUri: 'public/login', data: credentials })
  }

  localSingup(userInfo) {
    // return Promise.resolve({ data: { data: { status: 'OK' } } })
    return this.sendPost({ subUri: 'public/signup', data: userInfo })
  }

  changeUserInfo(userInfo) {
    return Promise.resolve({ data: { data: { status: 'OK' } } })
    // return this.sendPost({ subUri: 'public/singup', data: userInfo })
  }

  whoAmI() {
    // return Promise.resolve({ data: { data: { username: 'lkrazy', fullName: 'Lanh Dang' } } })
    return this.sendGet({ subUri: 'user/me' })
  }

  socialLogin(provider, code) {
    console.log('Code: ', code)
    return this.sendGet({ subUri: `oauth2/sso/${provider}/user_info?code=${code}` })
  }
}

export default new AuthenApi()
