import BaseApi from '@/api/base-api'

class AuthenApi extends BaseApi {
  constructor() {
    super('/api/community/community')
  }

  create(community) {
    return this.sendPost({ data: community })
  }

  modify(community) {
    return this.sendPut({ subUri: community.id, data: community })
  }

  updateCoverPhoto(communityId, coverPhoto) {
    return this.sendPost({ subUri: `${communityId}/coverPhoto`, data: coverPhoto })
  }

  getBySlug(slug) {
    return this.sendGet({ subUri: `slug/${slug}` })
  }

  getTrendingCommunities(query = { page: 0, pageSize: 10 }) {
    return this.sendGet({ subUri: '', query })
  }
}

export default new AuthenApi()
