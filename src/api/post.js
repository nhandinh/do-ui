import BaseApi from '@/api/base-api'

class AuthenApi extends BaseApi {
  constructor() {
    super('/api/community/post')
  }

  create(post, communityID, tagID) {
    return this.sendPost({ subUri: `${communityID}/${tagID}`, data: post })
  }

  getAllByCommunity(communityId, page = 0, limit = 10) {
    return this.sendGet({ subUri: communityId, query: { page, limit } })
  }

  getAll(page = 0, limit = 10) {
    return this.sendGet({ subUri: 1, query: { page, limit } })
  }

  upVote(communityId, postId) {
    return this.sendPost({ subUri: `${communityId}/${postId}/up-vote` })
  }
}

export default new AuthenApi()
