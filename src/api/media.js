import BaseApi from '@/api/base-api'

class MediaApi extends BaseApi {
  constructor() {
    super('/api/media')
  }

  upload(file) {
    return this.uploadFile({ subUri: 'upload', file })
  }
}

export default new MediaApi()
