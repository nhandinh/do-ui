import axios from 'axios'
import qs from 'qs'
import isEmpty from 'lodash/isEmpty'

class BaseApi {
  constructor(baseUri) {
    this.baseUri = baseUri
  }

  baseUri = ''

  buildUri(subUri, query) {
    let queryStr = ''
    if (!isEmpty(query)) {
      queryStr = `?${qs.stringify(query)}`
    }
    let subUriReal = ''
    if (subUri) {
      subUriReal = `/${subUri}`
    }
    return `${this.baseUri}${subUriReal}${queryStr}`
  }

  sendGet({ subUri, query }) {
    return axios.get(this.buildUri(subUri, query))
  }

  sendDelete({ subUri, query }) {
    return axios.delete(this.buildUri(subUri, query))
  }

  sendPost({ subUri, query, data }) {
    return axios.post(this.buildUri(subUri, query), data)
  }

  uploadFileBase({ subUri, query, data }) {
    const formData = new FormData()
    formData.append('file', data)
    return axios.post(this.buildUri(subUri, query), formData, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    })
  }

  sendPut({ subUri, query, data }) {
    return axios.put(this.buildUri(subUri, query), data)
  }

  uploadFile({ subUri, file }) {
    const formData = new FormData();
    formData.append('file', file)
    this.sendPost({ data: formData })
    return axios.post(this.buildUri(subUri), formData, {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })
  }
}

export default BaseApi
