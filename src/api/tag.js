import BaseApi from '@/api/base-api'

class TagApi extends BaseApi {
  constructor() {
    super('/api/community')
  }

  create(communityId, tag) {
    return this.sendPost({ subUri: `${communityId}/tag`, data: tag })
  }

  modify(communityId, tag) {
    return this.sendPut({ subUri: `${communityId}/tag`, data: tag })
  }

  getAll(comId) {
    return this.sendGet({ subUri: `${comId}/tag` })
  }
}

export default new TagApi()
