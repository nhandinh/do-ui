import Vue from 'vue'
import VueSocialAuth from 'vue-social-auth'

Vue.use(VueSocialAuth, {
  providers: {
    github: {
      clientId: 'c282e08ad402f9ec9469',
      redirectUri: 'http://doraemon.craftpa.com/app/auth/oauth2/github/callback'
    },
    google: {
      clientId: '344610943944-t5nl9jjrof0af998jn16knle93u6gdgi.apps.googleusercontent.com',
      redirectUri: 'http://doraemon.craftpa.com/app/auth/oauth2/google/callback'
    },
    facebook: {
      clientId: '2792141037495614',
      redirectUri: 'http://doraemon.craftpa.com/app/auth/oauth2/facebook/callback'
    }
  }
})
