import Vue from 'vue'
import Vuetify, {
  VAppBar,
  VAvatar,
  VBtn,
  VCard,
  VCol,
  VColorPicker,
  VImg,
  VList,
  VListItem,
  VListItemAction,
  VListItemAvatar,
  VListItemContent,
  VListItemGroup,
  VListItemIcon,
  VListItemTitle,
  VMenu,
  VMenuTransition,
  VProgressCircular,
  VTextarea,
  VToolbar,
  VInput,
  VIcon,
  VSlideYTransition,
  VSlideXTransition,
  VExpandXTransition,
  VScaleTransition,
  VFadeTransition,
  VDatePicker,
  VSnackbar
} from 'vuetify/lib'
import {
  Ripple,
  Scroll
} from 'vuetify/lib/directives'
import '@mdi/font/css/materialdesignicons.css'
import 'roboto-fontface'
import theme from './theme'


Vue.use(Vuetify, {
  components: {
    VAppBar,
    VAvatar,
    VBtn,
    VCard,
    VCol,
    VColorPicker,
    VImg,
    VList,
    VListItem,
    VListItemAction,
    VListItemAvatar,
    VListItemContent,
    VListItemGroup,
    VListItemIcon,
    VListItemTitle,
    VMenu,
    VMenuTransition,
    VProgressCircular,
    VTextarea,
    VToolbar,
    VInput,
    VIcon,
    VSlideYTransition,
    VSlideXTransition,
    VExpandXTransition,
    VScaleTransition,
    VFadeTransition,
    VDatePicker,
    VSnackbar
  },
  directives: {
    Ripple,
    Scroll
  },
  iconfont: 'mdi',
  theme
})

export default new Vuetify({
  theme: {
    options: {
      customProperties: true,
    },
    // themes: {
    //   light: {
    //     primary: '#ee44aa',
    //     secondary: '#424242',
    //     accent: '#82B1FF',
    //     error: '#FF5252',
    //     info: '#2196F3',
    //     success: '#5cb860',
    //     warning: '#FFC107',
    //   },
    // },
  },
})
