import isEmpty from 'lodash/isEmpty'
import store from '@/store'
import authenApi from '@/api/authen'

const token = localStorage.getItem('token')
if (isEmpty(token)) {
  setTimeout(() => {
    store.commit('app/setReady', true)
  }, 500)
} else {
  authenApi.whoAmI()
    .then((response) => {
      store.commit('authen/setUserInfo', response.data.data)
      store.commit('authen/setAuthenticated', true)
      store.commit('app/setReady', true)
    })
    .catch(() => {
      localStorage.removeItem('token')
      store.commit('app/setReady', true)
    })
}
