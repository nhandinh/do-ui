import Vue from 'vue'
import { extend, ValidationProvider, ValidationObserver } from 'vee-validate'
import { required, email } from 'vee-validate/dist/rules'

Vue.component('VValidate', ValidationProvider)
Vue.component('VValidateGroup', ValidationObserver)
extend('email', email)
extend('required', required)
