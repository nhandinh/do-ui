import Vue from 'vue'
import axios from 'axios'

Vue.prototype.$http = axios

if (process.env.NODE_ENV === 'development') {
  axios.defaults.baseURL = 'http://doraemon.craftpa.com'
}

axios.defaults.timeout = 150000
axios.defaults.headers.post['Content-Type'] = 'application/json'

axios.defaults.headers.post['Content-Type'] = 'application/json'

axios.interceptors.request.use((config) => {
  if (localStorage.getItem('token')) {
    config.headers.Authorization = `Bearer ${localStorage.getItem('token')}`.replace(/(^")|("$)/g, '')
  }
  return config
}, err => Promise.reject(err))
