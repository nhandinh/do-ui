import Vue from 'vue'
import { sync } from 'vuex-router-sync'

// Components
import '@/components'

// Plugins
import '@/plugins'
import vuetify from '@/plugins/vuetify'

// Application imports
import App from './App'
import router from '@/router'
import store from '@/store'

// Sync store with router
sync(store, router)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
